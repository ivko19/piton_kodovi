from csv import reader, writer
# Other approach, with only 1 file open at a time
with open('fighters.csv', 'r') as file:
    csv_reader = reader(file)
    n=[]
    for row in csv_reader:
        k = []
        for s in row:
            k.append(s.upper())
        n.append(k)
    print(n)

    # fighters = [[s.upper() for s in row] for row in csv_reader]
    # print(fighters)
