# You are given an n x n 2D matrix representing an image.
# Rotate the image by 90 degrees (clockwise).
# Note:
# You have to rotate the image in-place, which means you have to modify the input 2D matrix directly. DO NOT allocate another 2D matrix and do the rotation.
#
# izvorišna:
# ["A0", "A1", "A2", "A3"],
# ["B0", "B1", "B2", "B3"],
# ["C0", "C1", "C2", "C3"],
# ["D0", "D1", "D2", "D3"]
#
# odredišna:
# ["D0", "C0", "B0", "A0"],
# ["D1", "C1", "B1", "A1"],
# ["D2", "C2", "B2", "A2"],
# ["D3", "C3", "B3", "A3"]


# KOD SA NETA LIST COMPREHENSION
A=[["A0", "A1", "A2", "A3"], ["B0", "B1", "B2", "B3"], ["C0", "C1", "C2", "C3"], ["D0", "D1", "D2", "D3"]]
A[:] = [[row[i] for row in A[::-1]] for i in range(len(A))]
print(A)
print("ende comprehension")
print("")

# MOJ KOD - DUPLA PETLJA
dvolista=[["A0", "A1", "A2", "A3"], ["B0", "B1", "B2", "B3"], ["C0", "C1", "C2", "C3"], ["D0", "D1", "D2", "D3"]]
dvolista=dvolista[::-1]

twolista=[]
for i in range(len(dvolista)):
    twolista2=[]
    for row in range(len(dvolista)):
        twolista2.append(dvolista[row][i])
    twolista.append(twolista2)
print(twolista)