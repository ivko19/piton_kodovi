# Given a matrix of M x N elements (M rows, N columns), return all elements of the matrix in diagonal order as shown in the below image.
#
# Input:
#  [ 1, 2, 3 ],
#  [ 4, 5, 6 ],
#  [ 7, 8, 9 ]
#
# Output:  [1,2,4,7,5,3,6,8,9]

import csv

## given dimensions n,m --> overriden with csv file
# print("daj dimenziju x:")
# n = int(input())
# print("daj dimenziju y:")
# m = int(input())


## ucitavanje csv fajla
mxlista = []
with open("pravougaonik.csv", 'r') as data_file:
    reader = csv.reader(data_file, delimiter=',')
    for row in reader:
        mxlista.append(row[:])
# print(f"lista vrednosti iz csv:{mxlista}")

n=len(mxlista[0])
m=len(mxlista)

# creating headers of lists of positions
cl1 = [[(x,0)] for x in range(n)]
cl2 = [[(n-1,y)] for y in range(1,m)]

#creating list from horizontal headers
for index in range(len(cl1)):
    for ivko in range(cl1[index][0][0]):
        if ivko+1 > m-1:
            break
        cl1[index].append((cl1[index][0][0]-ivko-1,cl1[index][0][1]+ivko+1))
#creating list from vertical headers
for index in range(len(cl2)):
    for ivko in range(0,m-2):
        if ((cl2[index][0][1]+ivko+1) > m-1) or ((cl2[index][0][0]-ivko-1) < 0):
                break
        cl2[index].append((cl2[index][0][0]-ivko-1,cl2[index][0][1]+ivko+1))
#join two lists
cl1.extend(cl2)

#inversion of every second row
lista_kljuceva_matrice=[]
count=0
for element in cl1:
    if count % 2 == 1:
        element.reverse()
    count+=1
    lista_kljuceva_matrice.append(element)

# generisanje adresa u matrici
Level1=[]
for g in range(len(lista_kljuceva_matrice)):
    for h in range(len(lista_kljuceva_matrice[g])):
        Level1.append(lista_kljuceva_matrice[g][h])
# print(f"Level1: {Level1}")

# recnik pozicija:vrednost pocetna postavka
recnik_redom={}
for y in range(m):
    for x in range(n):
        recnik_redom.update(dict(zip([(x, y)], mxlista[y][x])))

# sortiranje elemenata korišćenjem recnika gore
#  klasična petlja prevedena u comprehension
# Level2=[]
# for tup in Level1:
#     Level2.append(recnik_redom[tup])
Level2 = [ recnik_redom[tup] for tup in Level1 ]
print(f"L2{Level2}")

