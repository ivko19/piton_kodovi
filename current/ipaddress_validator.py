# check if presented ip address is valid
# ^([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])$
import re

ipv4_string=r'([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])'
print("unesi mi ip adresu: ")
adresa = str(input())

print(f"adresa je:{adresa}")

def check_ipv4(adresa):
	pattern = re.compile(r'^('+ipv4_string+r')\.('+ipv4_string+r')\.('+ipv4_string+r')\.('+ipv4_string+r')$')
	res=pattern.search(adresa)
	if res:
		return res.group()
	return "This is not IPv4 address"

print(f"check: {check_ipv4(adresa)}")

print("unesi mi ip adresu: ")
inter = int(input())
print(inter)