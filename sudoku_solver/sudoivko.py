import csv
import itertools

## ucitavanje csv fajla
xlista = []
with open("sudoku.csv", 'r') as data_file:
    reader = csv.reader(data_file, delimiter=',')
    for row in reader:
        xlista.append(row[0:9])
print("lista vrednosti iz csv")
print(xlista)

## recnik sa tuple-ovima pozicija kao kljucevima
prazan = {}
popunjen = {}
pretpostavke = {}
recnik = {}
for x in range(9):
    for y in range(9):
        recnik.update(dict(zip([(x, y)], [xlista[x][y]])))

print("recnik sa tuple-ovima pozicija kao kljucevima")
print(recnik)

## kreiranje praznih i punih polja u dva recnika

for key, value in recnik.items():
    if value != '':
        popunjen.update({key: value})
    else:
        prazan.update({key: value})

pretpostavke.update(prazan)
for keys in pretpostavke.keys():
    pretpostavke[keys]=['1','2','3','4','5','6','7','8','9']

print()
print("kreiranje praznih, punih i pretpostavljenih polja u dva rečnika:")
print(f"prazna polja{prazan}")
print(f"popunjena polja{popunjen}")
print(f"pretpostavljena polja{pretpostavke}")
## liste relevantne za prazna polja
glob_lokalni_kvadrat = {}
glob_lokalni_osa_x = {}
glob_lokalni_osa_y = {}
glob_susedni_kvadrati_pravac_x = {}
glob_susedni_kvadrati_pravac_y = {}
glob_lokalni_kvadrat_pravac_x = {}
glob_lokalni_kvadrat_pravac_y = {}

print()
print("obrada pozicija")

for pozicija in prazan.keys():
# for pozicija in praznalistapozicija:
    ## prolazimo kroz svaku poziciju koja je iz tuple pretvorena u listu
    # print(f"obrada pozicije {pozicija}")
    pozicijaL = list(pozicija)
    lokalni_osa_x = []
    lokalni_osa_y = []
    lokalni_kvadrat = []
    susedni_kvadrati_pravac_x = []
    susedni_kvadrati_pravac_y = []
    lokalni_kvadrat_pravac_x = []
    lokalni_kvadrat_pravac_y = []



    for x in range(9):
        for y in range(9):
            ## ako je x/y u mini kvadratu, ali ne ta pozicija prazna pozicija onda je to mini kvadrat
            if (x // 3 == pozicijaL[0] // 3) and (x != pozicijaL[0]) and (y // 3 == pozicijaL[1] // 3) and (y != pozicijaL[1]):
                lokalni_kvadrat.append((x, y))

            ## ako je x/y u tom modulu a y/x nije u tom modulu i ovaj drugi nije u poziciji --> susedni kvadrat
            if (x // 3 == pozicijaL[0] // 3) and (x != pozicijaL[0]) and (y // 3 != pozicijaL[1] // 3) and (y != pozicijaL[1]):
                susedni_kvadrati_pravac_x.append((x, y))

            if (y // 3 == pozicijaL[1] // 3) and (y != pozicijaL[1]) and (x // 3 != pozicijaL[0] // 3) and (x != pozicijaL[0]):
                susedni_kvadrati_pravac_y.append((x, y))

            ## y/x nije pozicija, ali su oba u modulu
            if (x != pozicijaL[0]) and (x // 3 == pozicijaL[0] // 3) and (y==pozicijaL[1]):
                lokalni_kvadrat_pravac_y.append((x, pozicijaL[1]))

            if (y != pozicijaL[1])  and (y // 3 == pozicijaL[1] // 3) and (x==pozicijaL[0]):
                lokalni_kvadrat_pravac_x.append((pozicijaL[0], y))

            ## svih 8 pozicija u x/y pravcu
            if (y != pozicijaL[1]) and (x==pozicijaL[0]):
                lokalni_osa_x.append((pozicijaL[0], y))

            if (x != pozicijaL[0]) and (y==pozicijaL[1]):
                lokalni_osa_y.append((x, pozicijaL[1]))

    ## štampa svih vredonosti za datu poziciju
    # print(f"lokalni_kvadrat: {lokalni_kvadrat}")
    # print(f"susedni_kvadrati_pravac_x: {susedni_kvadrati_pravac_x}")
    # print(f"susedni_kvadrati_pravac_y: {susedni_kvadrati_pravac_y}")
    # print(f"lokalni_kvadrat_pravac_x: {lokalni_kvadrat_pravac_x}")
    # print(f"lokalni_kvadrat_pravac_y: {lokalni_kvadrat_pravac_y}")
    # print(f"lokalni_osa_x: {lokalni_osa_x}")
    # print(f"lokalni_osa_y: {lokalni_osa_y}")

    # udictovanje svih vrednosti za datu poziciju
    glob_lokalni_kvadrat.update(dict(zip([pozicija],[lokalni_kvadrat])))
    glob_lokalni_osa_x.update(dict(zip([pozicija],[lokalni_osa_x])))
    glob_lokalni_osa_y.update(dict(zip([pozicija],[lokalni_osa_y])))
    glob_susedni_kvadrati_pravac_x.update(dict(zip([pozicija],[susedni_kvadrati_pravac_x])))
    glob_susedni_kvadrati_pravac_y.update(dict(zip([pozicija],[susedni_kvadrati_pravac_y])))
    glob_lokalni_kvadrat_pravac_x.update(dict(zip([pozicija],[lokalni_kvadrat_pravac_x])))
    glob_lokalni_kvadrat_pravac_y.update(dict(zip([pozicija],[lokalni_kvadrat_pravac_y])))



#stampamo sada sve ovo...
print("Liste relevantne za prazna polja:")
print(f"lokalni_kvadrat: {glob_lokalni_kvadrat}")
print("ose x/y")
print(f"susedni_kvadrati_pravac_x: {glob_susedni_kvadrati_pravac_x}")
print(f"susedni_kvadrati_pravac_y: {glob_susedni_kvadrati_pravac_y}")
print("ose x/y")
print(f"lokalni_kvadrat_pravac_x: {glob_lokalni_kvadrat_pravac_x}")
print(f"lokalni_kvadrat_pravac_y: {glob_lokalni_kvadrat_pravac_y}")
print("ose x/y")
print(f"lokalni_osa_x: {glob_lokalni_osa_x}")
print(f"lokalni_osa_y: {glob_lokalni_osa_y}")


# sada ćemo joinovati vrednosti mogućnosti iz ovih dictova gore
glob_lokalni_kvadrat_mogucnosti = {}
for prazno_polje in prazan.keys():
#opet - za svako prazno polje mi gledamo
# prvo izbacujemo opcije iz lokalnog kvadrata

    lista_1_9 = ['1','2','3','4','5','6','7','8','9']
    # print('izbacivanje iz lokalnog kvadrata')
    # gledamo pozicije u ovom prvom dictu i proveravamo vrednosti u rečniku.
    for polje in glob_lokalni_kvadrat[prazno_polje]:
        #ako pozicija nije prazna - vrednost u recniku --> uklanjamo polje iz liste9
        if recnik[polje] != '':
            #        if recnik[polje] in lista_1_9:    # ovim vi se mogla iybeci kolizija  - ako nije u listi ni ne oduzimam ga
            lista_1_9.remove(recnik[polje])
    # na kraju mi u novi dict stavljamo mogućnosti za to prazno polje a ovde na osnovu lokalnog kvadrata
    glob_lokalni_kvadrat_mogucnosti.update(dict(zip({prazno_polje},[lista_1_9])))

#pa izbacujemo duz x ose
glob_lokalni_osa_x_mogucnosti = {}
for prazno_polje in prazan.keys():    # ovo je sigurno glup sistem, ali verovatno ću pozivati fje umesto ovih sekvencijalnih skripti
# gledamo pozicije u ovom drugom dictu
    lista_1_9 = ['1','2','3','4','5','6','7','8','9']
    # print('izbacivanje iz x-ose')
    for polje in glob_lokalni_osa_x[prazno_polje]:
        # print(recnik[polje])
        if recnik[polje] != '':
            lista_1_9.remove(recnik[polje])
    # na kraju mi u novi dict stavljamo mogućnosti za to prazno polje a ovde na osnovu lokalnog kvadrata
    glob_lokalni_osa_x_mogucnosti.update(dict(zip({prazno_polje},[lista_1_9])))

#pa izbacujemo duz y ose
glob_lokalni_osa_y_mogucnosti = {}
for prazno_polje in prazan.keys():
# gledamo pozicije u ovom prvom dictu
    lista_1_9 = ['1','2','3','4','5','6','7','8','9']
    # print('izbacivanje iz y-ose')
    for polje in glob_lokalni_osa_y[prazno_polje]:
        # print(recnik[polje])
        if recnik[polje] != '':
            lista_1_9.remove(recnik[polje])
    # na kraju mi u novi dict stavljamo mogućnosti za to prazno polje a ovde na osnovu lokalnog kvadrata
    glob_lokalni_osa_y_mogucnosti.update(dict(zip({prazno_polje},[lista_1_9])))

# štampam ove dictove potencijalnih vrednosti za prazne pozicije koje sam dobio
print(f"lokalni kvadrat mogucnosti:   {glob_lokalni_kvadrat_mogucnosti}")
print(f"Xosa mogucnosti:      {glob_lokalni_osa_x_mogucnosti}")
print(f"Yosa mogucnosti:      {glob_lokalni_osa_y_mogucnosti}")


#i sada dajem presek njih
lokalni_presek = {}
lokalni_presek_lista = []
for polje in prazan.keys():
    #presek po polju
    lokalni_presek_lista = [value for value in glob_lokalni_kvadrat_mogucnosti[polje] if value in glob_lokalni_osa_x_mogucnosti[polje] and value in glob_lokalni_osa_y_mogucnosti[polje]]
    lokalni_presek.update(dict(zip({polje},[lokalni_presek_lista])))
print()
print(f"lokalni_presek:    {lokalni_presek}")
# pravim duplikat koji vrtim za dužine manje od 2
lokalni_presek_temp={}
lokalni_presek_temp.update(lokalni_presek)
# print("lokalni_presektemp")
# print(lokalni_presek_temp)
print("lokalni_presek izbacivanje")
for kljuc in lokalni_presek_temp.keys():
    if len(lokalni_presek[kljuc])<2:
        # print("izbaceni kljuc")
        # print(kljuc)
        prazan.pop(kljuc)
        lokalni_presek.pop(kljuc)
        #vadim polje iz praznog
        popunjen.update(dict(zip({kljuc}, lokalni_presek_temp[kljuc])))
        # print("prazan/pun")
        # print(prazan)
        # print(popunjen)

# ovde je ono što ostaje neodređeno
print()
print("lokalni_presek sa izbačenim")
print(lokalni_presek)
print("prazan/pun")
print(prazan)
print(popunjen)
print("recnik")
recnik.update(popunjen)
recnik.update(prazan)
print(recnik)
if prazan.keys() == lokalni_presek.keys():
    print()
    print("############ključevi za prazan i presek su u sinku!!!################")
    print()
for keys in popunjen.keys():
    if recnik[key] != popunjen[key]:
        print("############vrednosti za rečnik i popunjen nisu u sinku!!!################")

print("bruteforce deo")
list_brute = []
# da napravimo listu
for kljuc,vrednost in lokalni_presek.items():
    print(lokalni_presek[kljuc])
    list_brute.append(lokalni_presek[kljuc])
# print(list_brute)


# sad pravim kombinaciju svih listi, ali dobijam listu tupleova
brutples=list(itertools.product(*list_brute))
print(f"brutalna lista tupleova: {brutples}")
# zato uvodim brista - listu listi umesto listu tupleova
brista=[]
for tup in brutples:
    brista.append(list(tup))
print(f"brutalna lista listi: {brista}")
# print()
# print(f"broj listi za obradu je:{len(brista)}")
# print()
## dovde lepo dokumentovano

proba = {}
# prazna_konacna=[]
resenje=[]
lista_1_9 =['1','2','3','4','5','6','7','8','9']
# for key in prazan.keys():
#     prazna_konacna.append(key)
# print(f"kljucevi{prazna_konacna}")

for element in brista:
    listanje = []
    lista_keys =[]
    for keys in prazan.keys():
        lista_keys.append(keys)
    lista_element = []
    for clan in element:
            # print(f"clan   {clan}")
        lista_element.append(clan)
    proba.update(dict(zip(lista_keys,lista_element)))
    # print(f" proba: {proba}")
    recnik.update(proba)
    # print("recnik probam")
    # print(recnik)
    for value in recnik.values():
        listanje.append(value)
        # for key, value in recnik.items():
        #      print(f"K {key}")
        #      print(f"V {value}")
        #      listanje.append(recnik[key])
    # print(f"listanje{listanje}")
    if (listanje.count('1') == 9) and (listanje.count('2') == 9) and (listanje.count('3') == 9) and (
            listanje.count('4') == 9) and (listanje.count('5') == 9) and (listanje.count('6') == 9) and (
            listanje.count('7') == 9) and (listanje.count('8') == 9) and (listanje.count('9') == 9):
        resenje.append(listanje)
        # print(f"listanje {listanje}")

    # for elementi in ['1','2','3','4','5','6','7','8','9']:
        # print(f"broj ponavljanja:  {elementi}    je  {listanje.count(elementi)}")
        # if listanje.count(elementi) != 9 :
        #     break
        # else:
        #     resenje.append(listanje)
            # print(f"listanje {listanje}")



for element in resenje:
    for elementi in ['1','2','3','4','5','6','7','8','9']:
        # for cen in lista_1_9:

        print(elementi)
        if element.count(elementi) != 9:
            print(f"broj ponavljanja:  {elementi}  je  {resenje.count(elementi)}")
            # break

        if element.count(elementi) == 0:
            print(f"RESHENJA {element}")
            print()


print(f"broj listi za obradu je: {len(brista)}")
print(f"broj resenja za obradu je: {len(resenje)}")
# for i in resenje:
#     print(i)
#     if .count(elementi) != 9:


print(resenje[0])