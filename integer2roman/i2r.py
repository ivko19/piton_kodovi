# Given an integer, convert it to a roman numeral. Input is guaranteed to be within the range from 1 to 3999.

listaj=["","I","II","III","IV","V","VI","VII","VIII","IX"]
listad=["","X","XX","XXX","XL","L","LX","LXX","LXXX","XC"]
listah=["","C","CC","CCC","CD","D","DC","DCC","DCCC","CM"]
listak=["","M","MM","MMM"]

print("unesite broj izmedju 1 i 3999: ")
broj = int(input())

#dobijanje težinskih cifara, vađenje iz listi,  join i štampa

rstring="".join(string for string in [listak[broj//1000],listah[(broj%1000)//100],listad[(broj%100)//10],listaj[broj%10]])

print (f"unet je broj: {broj}, sto je rimsko: {rstring}")